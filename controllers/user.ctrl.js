var express = require('express');

var ctrl = function () {
  var userCtrl = express.Router();

  userCtrl.route('/')
    .get(function (req, res) {
      return res.json({
        message: 'Easy peasy lemon squeezy'
      });
    })
    .post(function (req, res) {
      console.log(req.body);
      var message = req.body.message;
      res.json({
        message: message
      });
    });

  return userCtrl;
};

module.exports = ctrl;
